<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Client;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Client\Client;
use LaptopDev\MicsApi\Client\Reserve;
use LaptopDev\MicsApi\Request\Reserve\AddGoodsReserveRequest;
use LaptopDev\MicsApi\Request\Reserve\EditGoodsReserveRequest;
use LaptopDev\MicsApi\Request\Reserve\GetContractsRequest;
use LaptopDev\MicsApi\Request\Reserve\GetReserveInfoRequest;
use LaptopDev\MicsApi\Request\Reserve\GetStocksInfoRequest;
use LaptopDev\MicsApi\Request\Reserve\RemoveGoodsReserveRequest;
use LaptopDev\MicsApi\Response\Reserve\AddGoodsReserveResponse;
use LaptopDev\MicsApi\Response\Reserve\EditGoodsReserveResponse;
use LaptopDev\MicsApi\Response\Reserve\GetContractsResponse;
use LaptopDev\MicsApi\Response\Reserve\GetReserveInfoResponse;
use LaptopDev\MicsApi\Response\Reserve\GetStocksInfoResponse;
use LaptopDev\MicsApi\Response\Reserve\RemoveGoodsReserveResponse;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

class ReserveTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    public function setUp(): void
    {
        $this->httpClient = Mockery::mock(ClientInterface::class);
        $this->serializer = Mockery::mock(SerializerInterface::class);
        $this->login = 'test_login';
        $this->password = 'test_password';
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetStocksInfoRequest(): void
    {
        $request = Mockery::mock(GetStocksInfoRequest::class);
        $response = Mockery::mock(GetStocksInfoResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Reserve(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetStocksInfoResponse::class,
            $reserve->sendGetStocksInfoRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetContractsRequest(): void
    {
        $request = Mockery::mock(GetContractsRequest::class);
        $response = Mockery::mock(GetContractsResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Reserve(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetContractsResponse::class,
            $reserve->sendGetContractsRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetReserveInfoRequest(): void
    {
        $request = Mockery::mock(GetReserveInfoRequest::class);
        $response = Mockery::mock(GetReserveInfoResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Reserve(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetReserveInfoResponse::class,
            $reserve->sendGetReserveInfoRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendAddGoodsReserveRequest(): void
    {
        $request = Mockery::mock(AddGoodsReserveRequest::class);
        $response = Mockery::mock(AddGoodsReserveResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Reserve(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            AddGoodsReserveResponse::class,
            $reserve->sendAddGoodsReserveRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendEditGoodsReserveRequest(): void
    {
        $request = Mockery::mock(EditGoodsReserveRequest::class);
        $response = Mockery::mock(EditGoodsReserveResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Reserve(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            EditGoodsReserveResponse::class,
            $reserve->sendEditGoodsReserveRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendRemoveGoodsReserveRequest(): void
    {
        $request = Mockery::mock(RemoveGoodsReserveRequest::class);
        $response = Mockery::mock(RemoveGoodsReserveResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Reserve(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            RemoveGoodsReserveResponse::class,
            $reserve->sendRemoveGoodsReserveRequest($request)
        );
    }
}