<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Client;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Client\Client;
use LaptopDev\MicsApi\Client\Order;
use LaptopDev\MicsApi\Request\Order\DeleteOrderRequest;
use LaptopDev\MicsApi\Request\Order\GetConsigneesRequest;
use LaptopDev\MicsApi\Request\Order\GetOrderPositionsRequest;
use LaptopDev\MicsApi\Request\Order\GetShippingAddressRequest;
use LaptopDev\MicsApi\Request\Order\MakeOrderRequest;
use LaptopDev\MicsApi\Request\Order\MakeQuickOrderRequest;
use LaptopDev\MicsApi\Response\Order\DeleteOrderResponse;
use LaptopDev\MicsApi\Response\Order\GetConsigneesResponse;
use LaptopDev\MicsApi\Response\Order\GetOrderPositionsResponse;
use LaptopDev\MicsApi\Response\Order\GetShippingAddressResponse;
use LaptopDev\MicsApi\Response\Order\MakeOrderResponse;
use LaptopDev\MicsApi\Response\Order\MakeQuickOrderResponse;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

class OrderTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    public function setUp(): void
    {
        $this->httpClient = Mockery::mock(ClientInterface::class);
        $this->serializer = Mockery::mock(SerializerInterface::class);
        $this->login = 'test_login';
        $this->password = 'test_password';
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendMakeOrderRequest(): void
    {
        $request = Mockery::mock(MakeOrderRequest::class);
        $response = Mockery::mock(MakeOrderResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            MakeOrderResponse::class,
            $reserve->sendMakeOrderRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendMakeQuickOrderRequest(): void
    {
        $request = Mockery::mock(MakeQuickOrderRequest::class);
        $response = Mockery::mock(MakeQuickOrderResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            MakeQuickOrderResponse::class,
            $reserve->sendMakeQuickOrderRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetConsigneesRequest(): void
    {
        $request = Mockery::mock(GetConsigneesRequest::class);
        $response = Mockery::mock(GetConsigneesResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetConsigneesResponse::class,
            $reserve->sendGetConsigneesRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetShippingAddressRequest(): void
    {
        $request = Mockery::mock(GetShippingAddressRequest::class);
        $response = Mockery::mock(GetShippingAddressResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetShippingAddressResponse::class,
            $reserve->sendGetShippingAddressRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendDeleteOrderRequest(): void
    {
        $request = Mockery::mock(DeleteOrderRequest::class);
        $response = Mockery::mock(DeleteOrderResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            DeleteOrderResponse::class,
            $reserve->sendDeleteOrderRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetOrderPositionsRequest(): void
    {
        $request = Mockery::mock(GetOrderPositionsRequest::class);
        $response = Mockery::mock(GetOrderPositionsResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $reserve = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetOrderPositionsResponse::class,
            $reserve->sendGetOrderPositionsRequest($request)
        );
    }
}