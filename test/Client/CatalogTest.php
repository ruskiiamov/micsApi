<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Client;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Client\Catalog;
use LaptopDev\MicsApi\Client\Client;
use LaptopDev\MicsApi\Request\Catalog\GetCatalogRequest;
use LaptopDev\MicsApi\Request\Catalog\GetGoodsInfoRequest;
use LaptopDev\MicsApi\Request\Catalog\GetHierarchyRequest;
use LaptopDev\MicsApi\Response\Catalog\GetCatalogResponse;
use LaptopDev\MicsApi\Response\Catalog\GetGoodsInfoResponse;
use LaptopDev\MicsApi\Response\Catalog\GetHierarchyResponse;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

class CatalogTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    public function setUp(): void
    {
        $this->httpClient = Mockery::mock(ClientInterface::class);
        $this->serializer = Mockery::mock(SerializerInterface::class);
        $this->login = 'test_login';
        $this->password = 'test_password';
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetHierarchyRequest(): void
    {
        $request = Mockery::mock(GetHierarchyRequest::class);
        $response = Mockery::mock(GetHierarchyResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetHierarchyResponse::class,
            $catalog->sendGetHierarchyRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetCatalogRequest(): void
    {
        $request = Mockery::mock(GetCatalogRequest::class);
        $response = Mockery::mock(GetCatalogResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetCatalogResponse::class,
            $catalog->sendGetCatalogRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetGoodsInfoRequest(): void
    {
        $request = Mockery::mock(GetGoodsInfoRequest::class);
        $response = Mockery::mock(GetGoodsInfoResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetGoodsInfoResponse::class,
            $catalog->sendGetGoodsInfoRequest($request)
        );
    }
}