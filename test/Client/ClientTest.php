<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Client;

use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Client\Client;
use LaptopDev\MicsApi\Contract\Request;
use LaptopDev\MicsApi\Contract\Response;
use LaptopDev\MicsApi\Exception\ErrorResponseException;
use LaptopDev\MicsApi\Exception\ResponseException;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

class ClientTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    /** @var Request */
    protected $request;

    /** @var ResponseInterface */
    protected $response;

    protected function setUp(): void
    {
        $this->httpClient = Mockery::mock(ClientInterface::class);
        $this->serializer = Mockery::mock(SerializerInterface::class);
        $this->login = 'test_login';
        $this->password = 'test_password';
        $this->request = Mockery::mock(Request::class);
        $this->response = Mockery::mock(ResponseInterface::class);
    }

    public function testSendRequest(): void
    {
        $this->request
            ->shouldReceive('setLogin')
            ->once()
            ->andReturnSelf();

        $this->request
            ->shouldReceive('setPassword')
            ->once();

        $this->httpClient
            ->shouldReceive('request')
            ->once()
            ->andReturn(
                Mockery::mock(ResponseInterface::class)
                    ->shouldReceive('getBody')
                    ->andReturn('{"k": 0}')
                    ->getMock()
            );

        $this->serializer
            ->shouldReceive('serialize')
            ->once()
            ->andReturn('');

        $this->serializer
            ->shouldReceive('deserialize')
            ->once()
            ->andReturn(
                Mockery::mock(Response::class)
            );

        $client = new class(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        ) extends Client {
            public function exposedSendRequest(Request $request): Response
            {
                return $this->sendRequest($request);
            }
        };

        $this->request
            ->shouldReceive('responseClassName')
            ->once();

        $client->exposedSendRequest($this->request);
    }

    public function testExtractOptions(): void
    {
        $serialized = '{"login":"test_login","password":"test_password","method":"test_method"}';

        $this->serializer
            ->shouldReceive('serialize')
            ->once()
            ->andReturn($serialized);

        $client = new class(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        ) extends Client {
            public function exposedExtractOptions(Request $request): array
            {
                return $this->extractOptions($request);
            }
        };

        $actualOptions = $client->exposedExtractOptions($this->request);
        $expectedOptions = [
            'headers' => ['Content-Type' => 'application/json'],
            'query' => ['data' => $serialized],
        ];

        $this->assertEquals(
            $expectedOptions,
            $actualOptions
        );
    }

    public function testDeserialize(): void
    {
        $this->serializer
            ->shouldReceive('deserialize')
            ->once()
            ->andReturn(
                Mockery::mock(Response::class)
            );

        $client = new class(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        ) extends Client {
            public function exposedDeserialize(Request $request, ResponseInterface $response): Response
            {
                return $this->deserialize($request, $response);
            }
        };

        $this->response
            ->shouldReceive('getBody')
            ->times(3)
            ->andReturn('{"k":0}');

        $this->request
            ->shouldReceive('responseClassName')
            ->once()
            ->andReturn(Response::class);

        $client->exposedDeserialize($this->request, $this->response);

        $this->response
            ->shouldReceive('getBody')
            ->once()
            ->andReturn('text');

        $this->expectException(ResponseException::class);

        $client->exposedDeserialize($this->request, $this->response);

        $this->response
            ->shouldReceive('getBody')
            ->once()
            ->andReturn('{"status":false}');

        $this->expectException(ErrorResponseException::class);

        $client->exposedDeserialize($this->request, $this->response);
    }

    public function testIsJsonResponseBody(): void
    {
        $client = new class (
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        ) extends Client {
            public function exposedIsJsonResponseBody(ResponseInterface $response): bool
            {
                return $this->isJsonResponseBody($response);
            }
        };

        $this->response
            ->shouldReceive('getBody')
            ->once()
            ->andReturn('{"k":0}');

        $trueResult = $client->exposedIsJsonResponseBody($this->response);

        $this->assertTrue($trueResult);

        $this->response
            ->shouldReceive('getBody')
            ->once()
            ->andReturn('text');

        $falseResult = $client->exposedIsJsonResponseBody($this->response);

        $this->assertFalse($falseResult);
    }

    public function testIsErrorResponse(): void
    {
        $client = new class (
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        ) extends Client {
            public function exposedIsErrorResponse(ResponseInterface $response): bool
            {
                return $this->isErrorResponse($response);
            }
        };

        $this->response
            ->shouldReceive('getBody')
            ->once()
            ->andReturn('{"k":0}');

        $trueResult = $client->exposedIsErrorResponse($this->response);

        $this->assertFalse($trueResult);

        $this->response
            ->shouldReceive('getBody')
            ->once()
            ->andReturn('{"status":false}');

        $falseResult = $client->exposedIsErrorResponse($this->response);

        $this->assertTrue($falseResult);
    }
}