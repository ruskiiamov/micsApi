<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Request\Catalog;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Request\Catalog\GetGoodsInfoRequest;
use PHPUnit\Framework\TestCase;

class GetGoodsInfoRequestTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testSerialization(): void
    {
        $data = [
            'login' => 'test_login',
            'password' => 'test_password',
            'method' => 'GetGoodsInfo',
            'goods' => [123, 321],
            'contract' => 'test_contract',
            'paymentcondition' => 'test_paymentcondition',
        ];

        $expectedSerialized = json_encode($data);

        $request = new GetGoodsInfoRequest($data['contract'], $data['goods']);
        $request
            ->setLogin($data['login'])
            ->setPassword($data['password'])
            ->setPaymentCondition($data['paymentcondition']);

        $serialized = $this->serializer->serialize($request, 'json');

        $this->assertEquals(
            $expectedSerialized,
            $serialized
        );
    }
}