<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Request\Catalog;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Request\Catalog\GetCatalogRequest;
use PHPUnit\Framework\TestCase;

class GetCatalogRequestTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testSerialization(): void
    {
        $data = [
            'login' => 'test_login',
            'password' => 'test_password',
            'method' => 'getCatalog',
            'contract' => 'test_contract',
            'paymentcondition' => 'test_paymentcondition',
            'group' => 'test_group',
            'vendor' => 'test_vendor',
            'page' => 1,
            'deferred_delivery' => true,
            'conditions_only' => false
        ];

        $expectedSerialized = json_encode($data);

        $request = new GetCatalogRequest($data['contract']);
        $request
            ->setLogin($data['login'])
            ->setPassword($data['password'])
            ->setPaymentCondition($data['paymentcondition'])
            ->setGroup($data['group'])
            ->setVendor($data['vendor'])
            ->setPage($data['page'])
            ->setDeferredDelivery($data['deferred_delivery'])
            ->setConditionsOnly($data['conditions_only']);

        $serialized = $this->serializer->serialize($request, 'json');

        $this->assertEquals(
            $expectedSerialized,
            $serialized
        );
    }
}