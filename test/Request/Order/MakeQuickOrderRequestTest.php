<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Request\Order;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Good;
use LaptopDev\MicsApi\Request\Order\MakeQuickOrderRequest;
use PHPUnit\Framework\TestCase;

class MakeQuickOrderRequestTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testSerialization(): void
    {
        $data = [
            'login' => 'test_login',
            'password' => 'test_password',
            'method' => 'MakeQuickOrder',
            'goods' => [
                ['id' => 123],
                ['id' => 321],
            ],
            'consignee' => 'test_consignee',
            'delivery' => 1,
            'shipping' => 'test_shipping',
            'contract' => 'test_contract',
            'paymentcondition' => 'test_paymentcondition',
            'mode' => 2,
            'comments' => 'test_comments',
        ];

        $expectedSerialized = json_encode($data);

        $good1 = new Good();
        $good1->setId($data['goods'][0]['id']);

        $good2 = new Good();
        $good2->setId($data['goods'][1]['id']);

        $request = new MakeQuickOrderRequest(
            $data['contract'],
            $data['consignee'],
            [$good1]
        );
        $request
            ->setLogin($data['login'])
            ->setPassword($data['password'])
            ->addGood($good2)
            ->setDelivery($data['delivery'])
            ->setShipping($data['shipping'])
            ->setPaymentCondition($data['paymentcondition'])
            ->setMode($data['mode'])
            ->setComments($data['comments']);

        $serialized = $this->serializer->serialize($request, 'json');

        $this->assertEquals(
            $expectedSerialized,
            $serialized
        );
    }
}