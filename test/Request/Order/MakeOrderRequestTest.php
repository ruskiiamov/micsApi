<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Request\Order;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Request\Order\MakeOrderRequest;
use PHPUnit\Framework\TestCase;

class MakeOrderRequestTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testSerialization(): void
    {
        $data = [
            'login' => 'test_login',
            'password' => 'test_password',
            'method' => 'MakeOrder',
            'consignee' => 'test_consignee',
            'delivery' => 1,
            'contract' => 'test_contract',
            'paymentcondition' => 'test_paymentcondition',
            'mode' => 2,
            'comments' => 'test_comments',
        ];

        $expectedSerialized = json_encode($data);

        $request = new MakeOrderRequest(
            $data['contract'],
            $data['consignee'],
            $data['delivery']
        );
        $request
            ->setLogin($data['login'])
            ->setPassword($data['password'])
            ->setPaymentCondition($data['paymentcondition'])
            ->setMode($data['mode'])
            ->setComments($data['comments']);

        $serialized = $this->serializer->serialize($request, 'json');

        $this->assertEquals(
            $expectedSerialized,
            $serialized
        );
    }
}