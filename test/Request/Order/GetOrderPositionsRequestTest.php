<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Request\Order;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Request\Order\GetOrderPositionsRequest;
use PHPUnit\Framework\TestCase;

class GetOrderPositionsRequestTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testSerialization(): void
    {
        $data = [
            'login' => 'test_login',
            'password' => 'test_password',
            'method' => 'GetOrderPositions',
            'order' => 'test_order',
        ];

        $expectedSerialized = json_encode($data);

        $request = new GetOrderPositionsRequest($data['order']);
        $request
            ->setLogin($data['login'])
            ->setPassword($data['password']);

        $serialized = $this->serializer->serialize($request, 'json');

        $this->assertEquals(
            $expectedSerialized,
            $serialized
        );
    }
}