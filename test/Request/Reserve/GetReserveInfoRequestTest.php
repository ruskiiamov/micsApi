<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Request\Reserve;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Request\Reserve\GetReserveInfoRequest;
use PHPUnit\Framework\TestCase;

class GetReserveInfoRequestTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testSerialization(): void
    {
        $data = [
            'login' => 'test_login',
            'password' => 'test_password',
            'method' => 'GetReserveInfo',
            'contract' => 'test_contract',
        ];

        $expectedSerialized = json_encode($data);

        $request = new GetReserveInfoRequest($data['contract']);
        $request
            ->setLogin($data['login'])
            ->setPassword($data['password']);

        $serialized = $this->serializer->serialize($request, 'json');

        $this->assertEquals(
            $expectedSerialized,
            $serialized
        );
    }
}