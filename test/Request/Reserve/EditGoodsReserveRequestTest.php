<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Request\Reserve;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Good;
use LaptopDev\MicsApi\Request\Reserve\EditGoodsReserveRequest;
use PHPUnit\Framework\TestCase;

class EditGoodsReserveRequestTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testSerialization(): void
    {
        $data = [
            'login' => 'test_login',
            'password' => 'test_password',
            'method' => 'EditGoodsReserve',
            'contract' => 'test_contract',
            'goods' => [
                ['id' => 123],
                ['id' => 321],
            ],
        ];

        $expectedSerialized = json_encode($data);

        $good1 = new Good();
        $good1->setId($data['goods'][0]['id']);
        $good2 = new Good();
        $good2->setId($data['goods'][1]['id']);

        $request = new EditGoodsReserveRequest($data['contract'], [$good1]);
        $request
            ->setLogin($data['login'])
            ->setPassword($data['password'])
            ->addGood($good2);

        $serialized = $this->serializer->serialize($request, 'json');

        $this->assertEquals(
            $expectedSerialized,
            $serialized
        );
    }
}