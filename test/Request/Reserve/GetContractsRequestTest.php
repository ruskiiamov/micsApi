<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Request\Reserve;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Request\Reserve\GetContractsRequest;
use PHPUnit\Framework\TestCase;

class GetContractsRequestTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testSerialization(): void
    {
        $data = [
            'login' => 'test_login',
            'password' => 'test_password',
            'method' => 'getContracts'
        ];

        $expectedSerialized = json_encode($data);

        $request = new GetContractsRequest();
        $request
            ->setLogin($data['login'])
            ->setPassword($data['password']);

        $serialized = $this->serializer->serialize($request, 'json');

        $this->assertEquals(
            $expectedSerialized,
            $serialized
        );
    }
}