<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Contact;
use LaptopDev\MicsApi\Common\ShippingAddress;
use PHPUnit\Framework\TestCase;

class ShippingAddressTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'code' => 'test_code',
            'country' => 'test_country',
            'region_name' => 'test_region_name',
            'city_name' => 'test_city_name',
            'street' => 'test_street',
            'house_no' => 'test_house_no',
            'deliv_addr_comment' => 'test_deliv_addr_comment',
            'ship_agent' => 'test_ship_agent',
            'ship_agent_term' => 'test_ship_agent_term',
            'contacts' => [
                [],
                [],
            ]
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            ShippingAddress::class,
            'json');

        $this->assertEquals(
            $data['code'],
            $response->code()
        );

        $this->assertEquals(
            $data['country'],
            $response->country()
        );

        $this->assertEquals(
            $data['region_name'],
            $response->regionName()
        );

        $this->assertEquals(
            $data['city_name'],
            $response->cityName()
        );

        $this->assertEquals(
            $data['street'],
            $response->street()
        );

        $this->assertEquals(
            $data['house_no'],
            $response->houseNo()
        );

        $this->assertEquals(
            $data['deliv_addr_comment'],
            $response->delivAddrComment()
        );

        $this->assertEquals(
            $data['ship_agent'],
            $response->shipAgent()
        );

        $this->assertEquals(
            $data['ship_agent_term'],
            $response->shipAgentTerm()
        );

        $this->assertCount(
            2,
            $response->contacts()
        );

        $this->assertInstanceOf(
            Contact::class,
            $response->contacts()[0]
        );

        $this->assertInstanceOf(
            Contact::class,
            $response->contacts()[1]
        );
    }
}