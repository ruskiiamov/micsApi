<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Consignee;
use LaptopDev\MicsApi\Common\PaymentConditions;
use PHPUnit\Framework\TestCase;

class ConsigneeTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'text' => 'test_text',
            'value' => 'test_value',
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            Consignee::class,
            'json');

        $this->assertEquals(
            $data['text'],
            $response->text()
        );

        $this->assertEquals(
            $data['value'],
            $response->value()
        );
    }
}