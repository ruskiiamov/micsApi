<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\FullStock;
use PHPUnit\Framework\TestCase;

class FullStockTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'НазваниеСклада' => 'test_stock_name',
			'Остаток' => 'test_rest',
			'ДоступноДляЗаказа' => 'test_available',
			'Цена' => 'test_price',
			'Валюта' => 'test_currency',
			'ОжидаемыйПриход' => 0,
            'ВидОценки' => 'test_estimation_type',
            'КодВидаОценки' => 'test_estimation_type_code',
            'ИмяВидаОценки' => 'test_estimation_type_name',
        ];

        $serialized = json_encode($data, JSON_UNESCAPED_UNICODE);

        $response = $this->serializer->deserialize(
            $serialized,
            FullStock::class,
            'json');

        $this->assertEquals(
            $data['НазваниеСклада'],
            $response->stockName()
        );

        $this->assertEquals(
            $data['Остаток'],
            $response->rest()
        );

        $this->assertEquals(
            $data['ДоступноДляЗаказа'],
            $response->available()
        );

        $this->assertEquals(
            $data['Цена'],
            $response->price()
        );

        $this->assertEquals(
            $data['Валюта'],
            $response->currency()
        );

        $this->assertEquals(
            $data['ОжидаемыйПриход'],
            $response->expectedIncome()
        );

        $this->assertEquals(
            $data['ВидОценки'],
            $response->estimationType()
        );

        $this->assertEquals(
            $data['КодВидаОценки'],
            $response->estimationTypeCode()
        );

        $this->assertEquals(
            $data['ИмяВидаОценки'],
            $response->estimationTypeName()
        );
    }
}