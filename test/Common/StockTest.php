<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Stock;
use PHPUnit\Framework\TestCase;

class StockTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'num' => 'test_num',
            'price' => 'test_price'
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            Stock::class,
            'json');

        $this->assertEquals(
            $data['num'],
            $response->num()
        );

        $this->assertEquals(
            $data['price'],
            $response->price()
        );
    }
}