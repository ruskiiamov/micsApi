<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\GoodInfo;
use LaptopDev\MicsApi\Common\Stock;
use PHPUnit\Framework\TestCase;

class GoodInfoTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'id' => 123,
            'stock' => [
                'test_stock' => []
            ],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            GoodInfo::class,
            'json');

        $this->assertEquals(
            $data['id'],
            $response->id()
        );

        $this->assertInstanceOf(
            Stock::class,
            $response->stock()['test_stock']
        );
    }
}