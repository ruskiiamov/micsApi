<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\PaymentConditions;
use PHPUnit\Framework\TestCase;

class PaymentConditionsTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'Prepayment' => 1,
            'CreditDays' => 10
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            PaymentConditions::class,
            'json');

        $this->assertEquals(
            $data['Prepayment'],
            $response->prepayment()
        );

        $this->assertEquals(
            $data['CreditDays'],
            $response->creditDays()
        );
    }
}