<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Contract;
use LaptopDev\MicsApi\Common\PaymentConditions;
use PHPUnit\Framework\TestCase;

class ContractTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'ContractSapCode' => 'test_ContractSapCode',
            'PayerSapCode' => 'test_PayerSapCode',
            'ContractName' => 'test_ContractName',
            'SalesOrg' => 'test_SalesOrg',
            'ContractCurrency' => 'test_ContractCurrency',
            'PaymentConditions' => [],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            Contract::class,
            'json');

        $this->assertEquals(
            $data['ContractSapCode'],
            $response->contractSapCode()
        );

        $this->assertEquals(
            $data['PayerSapCode'],
            $response->payerSapCode()
        );

        $this->assertEquals(
            $data['ContractName'],
            $response->contractName()
        );

        $this->assertEquals(
            $data['SalesOrg'],
            $response->salesOrg()
        );

        $this->assertEquals(
            $data['ContractCurrency'],
            $response->contractCurrency()
        );

        $this->assertInstanceOf(
            PaymentConditions::class,
            $response->paymentConditions()
        );
    }
}