<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\CatalogItem;
use LaptopDev\MicsApi\Common\FullStock;
use PHPUnit\Framework\TestCase;

class CatalogItemTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'КодТовараSAP' => 123,
            'КодГруппы' => 'test_group_code',
            'Категория' => 'test_category',
            'Подкатегория' => 'test_subcategory',
            'Партномер' => 'test_part_number',
            'Ean' => 'test_ean',
            'КраткоеНаименование' => 'test_short_name',
            'ПолноеНаименование' => 'test_full_name',
            'КраткоеОписание' => 'test_short_description',
            'КодБренда' => 'test_brand_code',
            'Бренд' => 'test_brand',
            'КодТипаТовара' => 321,
            'ТипТовара' => 'test_good_type',
            'Распродажа' => 'test_discount',
            'Новинка' => 'test_new',
            'Эксклюзив' => 'test_exclusive',
            'Описание' => 'test_description',
            'КодМодельногоРяда' => 'test_model_lone_code',
            'МодельныйРяд' => 'test_model_line',
            'Комплект' => 'test_kit',
            'СкладыНЕЗ_ПОВ_УП' => [
                'stock1' => [],
                'stock2' => [],
            ],
            'СкладыНЕЗ_ПОВ_УП_ИмяВидаОценки' => 'test_stock_damaged_estimation_type_name',
            'СкладыБРАК' => [
                'stock3' => [],
                'stock4' => [],
            ],
            'СкладыБРАК_ИмяВидаОценки' => 'test_stock_rejected_estimation_type_name',
            'Склады' => [
                'stock5' => [],
                'stock6' => [],
            ],
        ];

        $serialized = json_encode($data, JSON_UNESCAPED_UNICODE);

        $response = $this->serializer->deserialize(
            $serialized,
            CatalogItem::class,
            'json');

        $this->assertEquals(
            $data['КодТовараSAP'],
            $response->goodCodeSap()
        );

        $this->assertEquals(
            $data['КодГруппы'],
            $response->groupCode()
        );

        $this->assertEquals(
            $data['Категория'],
            $response->category()
        );

        $this->assertEquals(
            $data['Подкатегория'],
            $response->subCategory()
        );

        $this->assertEquals(
            $data['Партномер'],
            $response->partNumber()
        );

        $this->assertEquals(
            $data['Ean'],
            $response->ean()
        );

        $this->assertEquals(
            $data['КраткоеНаименование'],
            $response->shortName()
        );

        $this->assertEquals(
            $data['ПолноеНаименование'],
            $response->fullName()
        );

        $this->assertEquals(
            $data['КраткоеОписание'],
            $response->shortDescription()
        );

        $this->assertEquals(
            $data['КодБренда'],
            $response->brandCode()
        );

        $this->assertEquals(
            $data['Бренд'],
            $response->brand()
        );

        $this->assertEquals(
            $data['КодТипаТовара'],
            $response->goodTypeCode()
        );

        $this->assertEquals(
            $data['ТипТовара'],
            $response->goodType()
        );

        $this->assertEquals(
            $data['Распродажа'],
            $response->discount()
        );

        $this->assertEquals(
            $data['Новинка'],
            $response->new()
        );

        $this->assertEquals(
            $data['Эксклюзив'],
            $response->exclusive()
        );

        $this->assertEquals(
            $data['Описание'],
            $response->description()
        );

        $this->assertEquals(
            $data['КодМодельногоРяда'],
            $response->modelLineCode()
        );

        $this->assertEquals(
            $data['МодельныйРяд'],
            $response->modelLine()
        );

        $this->assertEquals(
            $data['Комплект'],
            $response->kit()
        );

        $this->assertCount(
            2,
            $response->stocksDamagedBox()
        );

        $this->assertInstanceOf(
            FullStock::class,
            $response->stocksDamagedBox()['stock1']
        );

        $this->assertInstanceOf(
            FullStock::class,
            $response->stocksDamagedBox()['stock2']
        );

        $this->assertEquals(
            $data['СкладыНЕЗ_ПОВ_УП_ИмяВидаОценки'],
            $response->stocksDamagedBoxEstimationTypeName()
        );

        $this->assertCount(
            2,
            $response->stocksRejected()
        );

        $this->assertInstanceOf(
            FullStock::class,
            $response->stocksRejected()['stock3']
        );

        $this->assertInstanceOf(
            FullStock::class,
            $response->stocksRejected()['stock4']
        );

        $this->assertEquals(
            $data['СкладыБРАК_ИмяВидаОценки'],
            $response->stocksRejectedEstimationTypeName()
        );

        $this->assertCount(
            2,
            $response->stocks()
        );

        $this->assertInstanceOf(
            FullStock::class,
            $response->stocks()['stock5']
        );

        $this->assertInstanceOf(
            FullStock::class,
            $response->stocks()['stock6']
        );
    }
}