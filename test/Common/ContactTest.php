<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Contact;
use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'NAME' => 'test_name',
            'PHONE' => 'test_phone'
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            Contact::class,
            'json');

        $this->assertEquals(
            $data['NAME'],
            $response->name()
        );

        $this->assertEquals(
            $data['PHONE'],
            $response->phone()
        );
    }
}