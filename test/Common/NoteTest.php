<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Note;
use PHPUnit\Framework\TestCase;

class NoteTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'PrepaymentOrderCode' => 'test_PrepaymentOrderCode',
            'CreditOrderCode' => 'test_CreditOrderCode'
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            Note::class,
            'json');

        $this->assertEquals(
            $data['PrepaymentOrderCode'],
            $response->prepaymentOrderCode()
        );

        $this->assertEquals(
            $data['CreditOrderCode'],
            $response->creditOrderCode()
        );
    }
}