<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Hierarchy;
use PHPUnit\Framework\TestCase;

class HierarchyTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'КодГруппы' => '00020',
            'Уровень' => '1',
            'Название' => 'Ноутбуки',
        ];

       $serialized = json_encode($data, JSON_UNESCAPED_UNICODE);

        $response = $this->serializer->deserialize(
            $serialized,
            Hierarchy::class,
            'json');

        $this->assertEquals(
            $data['КодГруппы'],
            $response->groupCode()
        );

        $this->assertEquals(
            $data['Уровень'],
            $response->level()
        );

        $this->assertEquals(
            $data['Название'],
            $response->name()
        );
    }
}