<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Position;
use PHPUnit\Framework\TestCase;

class PositionTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'id' => 'test_id',
            'PartNumber' => 'test_PartNumber',
            'Name' => 'test_Name',
            'TerminalId' => 'test_TerminalId',
            'Quality' => 'test_Quality',
            'QualityName' => 'test_QualityName',
            'Price' => 123.45,
            'Cost' => 678.98,
            'Currency' => 'test_Currency',
            'Quantity' => 12,
            'Reserved' => 21,
            'Measure' => 'test_measure',
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            Position::class,
            'json');

        $this->assertEquals(
            $data['id'],
            $response->id()
        );

        $this->assertEquals(
            $data['PartNumber'],
            $response->partNumber()
        );

        $this->assertEquals(
            $data['Name'],
            $response->name()
        );

        $this->assertEquals(
            $data['TerminalId'],
            $response->terminalId()
        );

        $this->assertEquals(
            $data['Quality'],
            $response->quality()
        );


        $this->assertEquals(
            $data['QualityName'],
            $response->qualityName()
        );

        $this->assertEquals(
            $data['Price'],
            $response->price()
        );

        $this->assertEquals(
            $data['Cost'],
            $response->cost()
        );

        $this->assertEquals(
            $data['Currency'],
            $response->currency()
        );

        $this->assertEquals(
            $data['Quantity'],
            $response->quantity()
        );

        $this->assertEquals(
            $data['Reserved'],
            $response->reserved()
        );


        $this->assertEquals(
            $data['Measure'],
            $response->measure()
        );
    }
}