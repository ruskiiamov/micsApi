<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Good;
use PHPUnit\Framework\TestCase;

class GoodTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'id' => 123,
            'quantity' => 321,
            'measure' => 'test_measure',
            'stock' => 'test_stock',
            'name' => 'test_name',
            'price' => 123.45,
            'currency' => 'test_currency',
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            Good::class,
            'json');

        $this->assertEquals(
            $data['id'],
            $response->id()
        );

        $this->assertEquals(
            $data['quantity'],
            $response->quantity()
        );

        $this->assertEquals(
            $data['measure'],
            $response->measure()
        );

        $this->assertEquals(
            $data['stock'],
            $response->stock()
        );

        $this->assertEquals(
            $data['name'],
            $response->name()
        );

        $this->assertEquals(
            $data['price'],
            $response->price()
        );

        $this->assertEquals(
            $data['currency'],
            $response->currency()
        );
    }

    public function testSerialization(): void
    {
        $data = [
            'id' => 789,
            'quantity' => 5,
            'stock' => 'test_stock',
            'quality' => 'test_quality',
        ];

        $expectedSerialized = json_encode($data);

        $good = new Good();
        $good
            ->setId($data['id'])
            ->setQuantity($data['quantity'])
            ->setQuality($data['quality'])
            ->setStock($data['stock']);

        $serialized = $this->serializer->serialize($good, 'json');

        $this->assertEquals(
            $expectedSerialized,
            $serialized
        );
    }
}