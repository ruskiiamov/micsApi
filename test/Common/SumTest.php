<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Common;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Sum;
use PHPUnit\Framework\TestCase;

class SumTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'currency' => 'test_currency',
            'sum' => 123.45
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            Sum::class,
            'json');

        $this->assertEquals(
            $data['currency'],
            $response->currency()
        );

        $this->assertEquals(
            $data['sum'],
            $response->sum()
        );
    }
}