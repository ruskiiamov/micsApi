<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Order;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Consignee;
use LaptopDev\MicsApi\Response\Order\GetConsigneesResponse;
use PHPUnit\Framework\TestCase;

class GetConsigneesResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => true,
            'errorMessage' => '',
            'consignees' => [
                [],
                [],
            ],
            'paymentConditions' => [
                1 => 'test_condition_1',
                0 => 'test_condition_2',
            ],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            GetConsigneesResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertEquals(
            $data['errorMessage'],
            $response->errorMessage()
        );

        $this->assertCount(
            2,
            $response->consignees()
        );

        $this->assertInstanceOf(
            Consignee::class,
            $response->consignees()[0]
        );

        $this->assertInstanceOf(
            Consignee::class,
            $response->consignees()[1]
        );

        $this->assertEquals(
            $data['paymentConditions'][1],
            $response->paymentConditions()[1]
        );

        $this->assertEquals(
            $data['paymentConditions'][0],
            $response->paymentConditions()[0]
        );
    }
}