<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Order;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Response\Order\DeleteOrderResponse;
use PHPUnit\Framework\TestCase;

class DeleteOrderResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => true,
            'order' => 'test_order',
            'message' => 'test_message',
            'errorMessage' => '',
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            DeleteOrderResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertEquals(
            $data['order'],
            $response->order()
        );

        $this->assertEquals(
            $data['message'],
            $response->message()
        );

        $this->assertEquals(
            $data['errorMessage'],
            $response->errorMessage()
        );
    }
}