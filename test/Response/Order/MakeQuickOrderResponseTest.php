<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Order;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Response\Order\MakeQuickOrderResponse;
use PHPUnit\Framework\TestCase;

class MakeQuickOrderResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => true,
            'errorMessage' => '',
            'orders' => ['order1', 'order2'],
            'message' => 'test_message',
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            MakeQuickOrderResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertEquals(
            $data['errorMessage'],
            $response->errorMessage()
        );

        $this->assertCount(
            2,
            $response->orders()
        );

        $this->assertEquals(
            $data['orders'][0],
            $response->orders()[0]
        );

        $this->assertEquals(
            $data['orders'][1],
            $response->orders()[1]
        );

        $this->assertEquals(
            $data['message'],
            $response->message()
        );
    }
}