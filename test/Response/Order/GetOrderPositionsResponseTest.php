<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Order;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Position;
use LaptopDev\MicsApi\Response\Order\GetOrderPositionsResponse;
use PHPUnit\Framework\TestCase;

class GetOrderPositionsResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => true,
            'errorMessage' => '',
            'positions' => [
                [],
                [],
            ],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            GetOrderPositionsResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertEquals(
            $data['errorMessage'],
            $response->errorMessage()
        );

        $this->assertCount(
            2,
            $response->positions()
        );

        $this->assertInstanceOf(
            Position::class,
            $response->positions()[0]
        );

        $this->assertInstanceOf(
            Position::class,
            $response->positions()[1]
        );
    }
}