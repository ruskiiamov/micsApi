<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Order;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\ShippingAddress;
use LaptopDev\MicsApi\Response\Order\GetShippingAddressResponse;
use PHPUnit\Framework\TestCase;

class GetShippingAddressResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => true,
            'errorMessage' => '',
            'shippingAddress' => [
                [],
                [],
            ],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            GetShippingAddressResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertEquals(
            $data['errorMessage'],
            $response->errorMessage()
        );

        $this->assertCount(
            2,
            $response->shippingAddress()
        );

        $this->assertInstanceOf(
            ShippingAddress::class,
            $response->shippingAddress()[0]
        );

        $this->assertInstanceOf(
            ShippingAddress::class,
            $response->shippingAddress()[1]
        );
    }
}