<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Response\ErrorResponse;
use PHPUnit\Framework\TestCase;

class ErrorResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => false,
            'errorCode' => 123,
            'errorText' => 'test_errorText',
            'message' => 'test_message',
            'errorMessage' => 'test_errorMessage',
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            ErrorResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertEquals(
            $data['errorCode'],
            $response->errorCode()
        );

        $this->assertEquals(
            $data['errorText'],
            $response->errorText()
        );

        $this->assertEquals(
            $data['message'],
            $response->message()
        );

        $this->assertEquals(
            $data['errorMessage'],
            $response->errorMessage()
        );
    }
}