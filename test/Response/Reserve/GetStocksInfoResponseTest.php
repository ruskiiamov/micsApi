<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Reserve;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Response\Reserve\GetStocksInfoResponse;
use PHPUnit\Framework\TestCase;

class GetStocksInfoResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => true,
            'stocks' => [
                '123' => 'test_name_1',
                '321' => 'test_name_2',
            ],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            GetStocksInfoResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertCount(
            2,
            $response->stocks());

        $this->assertEquals(
            $data['stocks']['123'],
            $response->stocks()['123']
        );

        $this->assertEquals(
            $data['stocks']['321'],
            $response->stocks()['321']
        );
    }
}