<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Reserve;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Response\Reserve\RemoveGoodsReserveResponse;
use PHPUnit\Framework\TestCase;

class RemoveGoodsReserveResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => true,
            'warning' => 'test_warning',
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            RemoveGoodsReserveResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertEquals(
            $data['warning'],
            $response->warning()
        );
    }
}