<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Reserve;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Contract;
use LaptopDev\MicsApi\Common\Note;
use LaptopDev\MicsApi\Response\Reserve\GetContractsResponse;
use PHPUnit\Framework\TestCase;

class GetContractsResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'Contracts' => [
                [],
                [],
            ],
            'Note' => [],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            GetContractsResponse::class,
            'json');

        $this->assertCount(
            2,
            $response->contracts());

        $this->assertInstanceOf(
            Contract::class,
            $response->contracts()[0]
        );

        $this->assertInstanceOf(
            Contract::class,
            $response->contracts()[1]
        );

        $this->assertInstanceOf(
            Note::class,
            $response->note()
        );
    }
}