<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Reserve;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Good;
use LaptopDev\MicsApi\Common\Sum;
use LaptopDev\MicsApi\Response\Reserve\AddGoodsReserveResponse;
use PHPUnit\Framework\TestCase;

class AddGoodsReserveResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => true,
            'sum' => [
                [],
                [],
            ],
            'goods' => [
                [],
                [],
            ],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            AddGoodsReserveResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertCount(
            2,
            $response->sum()
        );

        $this->assertInstanceOf(
            Sum::class,
            $response->sum()[0]
        );

        $this->assertInstanceOf(
            Sum::class,
            $response->sum()[1]
        );

        $this->assertCount(
            2,
            $response->goods()
        );

        $this->assertInstanceOf(
            Good::class,
            $response->goods()[0]
        );

        $this->assertInstanceOf(
            Good::class,
            $response->goods()[1]
        );
    }
}