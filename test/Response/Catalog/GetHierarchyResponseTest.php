<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Catalog;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\Hierarchy;
use LaptopDev\MicsApi\Response\Catalog\GetHierarchyResponse;
use PHPUnit\Framework\TestCase;

class GetHierarchyResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => true,
            'errorMessage' => '',
            'hierarchy' => [
                [],
                [],
            ],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            GetHierarchyResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertEquals(
            $data['errorMessage'],
            $response->errorMessage()
        );

        $this->assertCount(
            2,
            $response->hierarchy());

        $this->assertInstanceOf(
            Hierarchy::class,
            $response->hierarchy()[0]
        );

        $this->assertInstanceOf(
            Hierarchy::class,
            $response->hierarchy()[1]
        );
    }
}