<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Catalog;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\CatalogItem;
use LaptopDev\MicsApi\Response\Catalog\GetCatalogResponse;
use PHPUnit\Framework\TestCase;

class GetCatalogResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'catalog_item1' => [],
            'catalog_item2' => [],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            GetCatalogResponse::class,
            'json');

        $this->assertCount(
            2,
            $response->catalogItems());

        $this->assertInstanceOf(
            CatalogItem::class,
            $response->catalogItems()['catalog_item1']
        );

        $this->assertInstanceOf(
            CatalogItem::class,
            $response->catalogItems()['catalog_item2']
        );
    }
}