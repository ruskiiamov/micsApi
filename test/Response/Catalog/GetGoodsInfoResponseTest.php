<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi\Response\Catalog;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Common\GoodInfo;
use LaptopDev\MicsApi\Response\Catalog\GetGoodsInfoResponse;
use PHPUnit\Framework\TestCase;

class GetGoodsInfoResponseTest extends TestCase
{
    /** @var SerializerInterface */
    private $serializer;

    public function setUp(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    public function testDeserialize(): void
    {
        $data = [
            'status' => true,
            'goods' => [
                [],
                [],
            ],
        ];

        $serialized = json_encode($data);

        $response = $this->serializer->deserialize(
            $serialized,
            GetGoodsInfoResponse::class,
            'json');

        $this->assertEquals(
            $data['status'],
            $response->status()
        );

        $this->assertCount(
            2,
            $response->goods());

        $this->assertInstanceOf(
            GoodInfo::class,
            $response->goods()[0]
        );

        $this->assertInstanceOf(
            GoodInfo::class,
            $response->goods()[1]
        );
    }
}