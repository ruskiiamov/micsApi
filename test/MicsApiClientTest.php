<?php

declare(strict_types=1);

namespace LaptopDev\test\MicsApi;

use GuzzleHttp\Client as HttpClient;
use LaptopDev\MicsApi\Client\Catalog;
use LaptopDev\MicsApi\Client\Order;
use LaptopDev\MicsApi\Client\Reserve;
use LaptopDev\MicsApi\MicsApiClient;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

class MicsApiClientTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testConstruct(): void
    {
        Mockery::mock('overload:' . Catalog::class);
        Mockery::mock('overload:' . Reserve::class);
        Mockery::mock('overload:' . Order::class);

        $baseUri = 'test_uri';
        $timeout = 10;

        Mockery::mock('overload:' . HttpClient::class)
            ->shouldReceive('__construct')
            ->once()
            ->with([
                'base_uri' => $baseUri,
                'timeout' => $timeout
            ]);

        $micsApiClient = new MicsApiClient(
            $baseUri,
            'test_login',
            'test_password',
            $timeout
        );

        $this->assertInstanceOf(
            Catalog::class,
            $micsApiClient->catalog
        );

        $this->assertInstanceOf(
            Reserve::class,
            $micsApiClient->reserve
        );

        $this->assertInstanceOf(
            Order::class,
            $micsApiClient->order
        );
    }
}