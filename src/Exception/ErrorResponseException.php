<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Exception;

use Exception;
use LaptopDev\MicsApi\Contract\ErrorResponse;
use LaptopDev\MicsApi\Contract\Request;


class ErrorResponseException extends Exception
{
    /**
     * @var ErrorResponse
     */
    private $errorResponse;

    /**
     * @var Request
     */
    private $request;

    public function __construct(ErrorResponse $errorResponse, Request $request)
    {
        $requestNameArr = explode('\\', get_class($request));
        $requestName = array_pop($requestNameArr);

        $message = $errorResponse->errorText() ?? $errorResponse->errorMessage() ?? $errorResponse->message() ?? '';

        parent::__construct('MicsApi Response error: ' . $requestName .  ' - ' . $message);

        $this->errorResponse = $errorResponse;
        $this->request = $request;
    }

    /**
     * @return ErrorResponse
     */
    public function errorResponse(): ErrorResponse
    {
        return $this->errorResponse;
    }

    /**
     * @return Request
     */
    public function request(): Request
    {
        return $this->request;
    }
}