<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi;

use GuzzleHttp\Client as HttpClient;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Visitor\Factory\JsonDeserializationVisitorFactory;
use JMS\Serializer\Visitor\Factory\JsonSerializationVisitorFactory;
use LaptopDev\MicsApi\Client\Catalog;
use LaptopDev\MicsApi\Client\Order;
use LaptopDev\MicsApi\Client\Reserve;

class MicsApiClient
{
    /** @var Catalog */
    public $catalog;

    /** @var Reserve */
    public $reserve;

    /** @var Order */
    public $order;

    public function __construct(
        string $baseUri,
        string $login,
        string $password,
        int $timeout = 10
    ) {
        $httpClient = new HttpClient([
            'base_uri' => $baseUri,
            'timeout' => $timeout
        ]);

        $serializationVisitorFactory = new JsonSerializationVisitorFactory();
        $serializationVisitorFactory->setOptions(JSON_PRESERVE_ZERO_FRACTION | JSON_UNESCAPED_UNICODE);

        $deserializationVisitorFactory = new JsonDeserializationVisitorFactory();
        $deserializationVisitorFactory->setOptions(JSON_PRESERVE_ZERO_FRACTION | JSON_UNESCAPED_UNICODE);

        $serializer = SerializerBuilder::create()
            ->setSerializationVisitor('json', $serializationVisitorFactory)
            ->setDeserializationVisitor('json', $deserializationVisitorFactory)
            ->build();

        $this->catalog = new Catalog($httpClient, $serializer, $login, $password);
        $this->reserve = new Reserve($httpClient, $serializer, $login, $password);
        $this->order = new Order($httpClient, $serializer, $login, $password);
    }
}