<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Contract;

interface Request
{
    /**
     * @return string
     */
    public function responseClassName(): string;

    /**
     * @param string $login
     * @return $this
     */
    public function setLogin(string $login): self;

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self;

    /**
     * @return string
     */
    public function login(): string;

    /**
     * @return string
     */
    public function password(): string;
}