<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Contract;

interface ErrorResponse
{
    /**
     * @return string|null
     */
    public function errorText(): ?string;

    /**
     * @return string|null
     */
    public function errorMessage(): ?string;

    /**
     * @return string|null
     */
    public function message(): ?string;
}