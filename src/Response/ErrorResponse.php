<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response;

use JMS\Serializer\Annotation as JMS;
use \LaptopDev\MicsApi\Contract\ErrorResponse as ErrorResponseInterface;

class ErrorResponse implements ErrorResponseInterface
{
    /**
     * @JMS\SerializedName("status")
     * @JMS\Type("bool")
     *
     * @var bool
     */
    private $status;

    /**
     * @JMS\SerializedName("errorCode")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $errorCode;

    /**
     * @JMS\SerializedName("errorText")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $errorText;

    /**
     * @JMS\SerializedName("message")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $message;

    /**
     * @JMS\SerializedName("errorMessage")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $errorMessage;

    /**
     * @return bool
     */
    public function status(): bool
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function errorCode(): int
    {
        return $this->errorCode;
    }

    /**
     * @return string|null
     */
    public function errorText(): ?string
    {
        return $this->errorText;
    }

    /**
     * @return string|null
     */
    public function message(): ?string
    {
        return $this->message;
    }

    /**
     * @return string|null
     */
    public function errorMessage(): ?string
    {
        return $this->errorMessage;
    }
}