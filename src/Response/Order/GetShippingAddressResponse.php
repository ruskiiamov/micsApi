<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Common\ShippingAddress;
use LaptopDev\MicsApi\Response\AbstractResponse;

class GetShippingAddressResponse extends AbstractResponse
{
    /**
     * @JMS\SerializedName("errorMessage")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $errorMessage;

    /**
     * @JMS\SerializedName("shippingAddress")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\ShippingAddress>")
     *
     * @var ShippingAddress[]
     */
    private $shippingAddress;

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @return ShippingAddress[]
     */
    public function shippingAddress(): array
    {
        return $this->shippingAddress;
    }
}