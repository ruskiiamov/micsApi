<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Common\Position;
use LaptopDev\MicsApi\Response\AbstractResponse;

class GetOrderPositionsResponse extends AbstractResponse
{
    /**
     * @JMS\SerializedName("errorMessage")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $errorMessage;

    /**
     * @JMS\SerializedName("positions")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\Position>")
     *
     * @var Position[]
     */
    private $positions;

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @return Position[]
     */
    public function positions(): array
    {
        return $this->positions;
    }
}