<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Common\Consignee;
use LaptopDev\MicsApi\Response\AbstractResponse;

class GetConsigneesResponse extends AbstractResponse
{
    /**
     * @JMS\SerializedName("errorMessage")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $errorMessage;

    /**
     * @JMS\SerializedName("consignees")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\Consignee>")
     *
     * @var Consignee[]
     */
    private $consignees;

    /**
     * @JMS\SerializedName("paymentConditions")
     * @JMS\Type("array<int, string>")
     *
     * @var array
     */
    private $paymentConditions;

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @return Consignee[]
     */
    public function consignees(): array
    {
        return $this->consignees;
    }

    /**
     * @return array
     */
    public function paymentConditions(): array
    {
        return $this->paymentConditions;
    }
}