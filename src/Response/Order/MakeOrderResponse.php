<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Response\AbstractResponse;

class MakeOrderResponse extends AbstractResponse
{
    /**
     * @JMS\SerializedName("errorMessage")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $errorMessage;

    /**
     * @JMS\SerializedName("orders")
     * @JMS\Type("array<string>")
     *
     * @var array
     */
    private $orders;

    /**
     * @JMS\SerializedName("message")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $message;

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @return array
     */
    public function orders(): array
    {
        return $this->orders;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }
}