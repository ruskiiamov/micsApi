<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Response\AbstractResponse;

class DeleteOrderResponse extends AbstractResponse
{
    /**
     * @JMS\SerializedName("order")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $order;

    /**
     * @JMS\SerializedName("message")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $message;

    /**
     * @JMS\SerializedName("errorMessage")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $errorMessage;

    /**
     * @return string
     */
    public function order(): string
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }
}