<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Reserve;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Contract\Response;
use LaptopDev\MicsApi\Response\AbstractResponse;

class GetStocksInfoResponse extends AbstractResponse
{
    /**
     * @JMS\SerializedName("stocks")
     * @JMS\Type("array<string, string>")
     *
     * @var array
     */
    private $stocks;

    /**
     * @return array
     */
    public function stocks(): array
    {
        return $this->stocks;
    }
}