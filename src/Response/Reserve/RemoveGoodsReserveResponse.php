<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Reserve;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Response\AbstractResponse;

class RemoveGoodsReserveResponse extends AbstractResponse
{
    /**
     * @JMS\SerializedName("warning")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $warning;

    /**
     * @return string
     */
    public function warning(): string
    {
        return $this->warning;
    }
}