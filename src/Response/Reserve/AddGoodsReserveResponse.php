<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Reserve;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Common\Good;
use LaptopDev\MicsApi\Common\Sum;
use LaptopDev\MicsApi\Response\AbstractResponse;

class AddGoodsReserveResponse extends AbstractResponse
{
    /**
     * @JMS\SerializedName("sum")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\Sum>")
     *
     * @var Sum[]
     */
    private $sum;

    /**
     * @JMS\SerializedName("goods")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\Good>")
     *
     * @var Good[]
     */
    private $goods;

    /**
     * @return Sum[]
     */
    public function sum(): array
    {
        return $this->sum;
    }

    /**
     * @return Good[]
     */
    public function goods(): array
    {
        return $this->goods;
    }
}