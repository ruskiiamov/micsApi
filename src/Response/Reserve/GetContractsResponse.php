<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Reserve;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Common\Contract;
use LaptopDev\MicsApi\Common\Note;
use LaptopDev\MicsApi\Contract\Response;

class GetContractsResponse implements Response
{
    /**
     * @JMS\SerializedName("Contracts")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\Contract>")
     *
     * @var Contract[]
     */
    private $contracts;

    /**
     * @JMS\SerializedName("Note")
     * @JMS\Type("LaptopDev\MicsApi\Common\Note")
     *
     * @var Note
     */
    private $note;

    /**
     * @return Contract[]
     */
    public function contracts(): array
    {
        return $this->contracts;
    }

    /**
     * @return Note
     */
    public function note(): Note
    {
        return $this->note;
    }
}