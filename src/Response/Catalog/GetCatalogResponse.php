<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Common\CatalogItem;
use LaptopDev\MicsApi\Contract\Response;

class GetCatalogResponse implements Response
{
    /**
     * @JMS\Inline
     * @JMS\Type("array<string, LaptopDev\MicsApi\Common\CatalogItem>")
     *
     * @var CatalogItem[]
     */
    private $catalogItems;

    /**
     * @return CatalogItem[]
     */
    public function catalogItems(): array
    {
        return $this->catalogItems;
    }
}