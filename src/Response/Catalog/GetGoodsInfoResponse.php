<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Common\GoodInfo;
use LaptopDev\MicsApi\Response\AbstractResponse;

class GetGoodsInfoResponse extends AbstractResponse
{
    /**
     * @JMS\SerializedName("goods")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\GoodInfo>")
     *
     * @var GoodInfo[]
     */
    private $goods;

    /**
     * @return GoodInfo[]
     */
    public function goods(): array
    {
        return $this->goods;
    }
}