<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Common\Hierarchy;
use LaptopDev\MicsApi\Response\AbstractResponse;

class GetHierarchyResponse extends AbstractResponse
{
    /**
     * @JMS\SerializedName("errorMessage")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $errorMessage;

    /**
     * @JMS\SerializedName("hierarchy")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\Hierarchy>")
     *
     * @var Hierarchy[]
     */
    private $hierarchy;

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @return Hierarchy[]
     */
    public function hierarchy(): array
    {
        return $this->hierarchy;
    }
}