<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Response;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Contract\Response;

class AbstractResponse implements Response
{
    /**
     * @JMS\SerializedName("status")
     * @JMS\Type("bool")
     *
     * @var bool
     */
    private $status;

    /**
     * @return bool
     */
    public function status(): bool
    {
        return $this->status;
    }
}