<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Client;

use Exception;
use LaptopDev\MicsApi\Request\Catalog\GetCatalogRequest;
use LaptopDev\MicsApi\Request\Catalog\GetGoodsInfoRequest;
use LaptopDev\MicsApi\Request\Catalog\GetHierarchyRequest;
use LaptopDev\MicsApi\Response\Catalog\GetCatalogResponse;
use LaptopDev\MicsApi\Response\Catalog\GetGoodsInfoResponse;
use LaptopDev\MicsApi\Response\Catalog\GetHierarchyResponse;

class Catalog extends Client
{
    /**
     * @param GetHierarchyRequest $request
     * @return GetHierarchyResponse
     * @throws Exception
     */
    public function sendGetHierarchyRequest(GetHierarchyRequest $request): GetHierarchyResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetCatalogRequest $request
     * @return GetCatalogResponse
     * @throws Exception
     */
    public function sendGetCatalogRequest(GetCatalogRequest $request): GetCatalogResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetGoodsInfoRequest $request
     * @return GetGoodsInfoResponse
     * @throws Exception
     */
    public function sendGetGoodsInfoRequest(GetGoodsInfoRequest $request): GetGoodsInfoResponse
    {
        return $this->sendRequest($request);
    }
}