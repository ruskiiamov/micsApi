<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Client;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JMS\Serializer\SerializerInterface;
use LaptopDev\MicsApi\Contract\Request;
use LaptopDev\MicsApi\Contract\Response;
use LaptopDev\MicsApi\Exception\ErrorResponseException;
use LaptopDev\MicsApi\Exception\RequestException;
use LaptopDev\MicsApi\Exception\ResponseException;
use LaptopDev\MicsApi\Response\ErrorResponse;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

abstract class Client
{
    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    /**
     * @param ClientInterface $httpClient
     * @param SerializerInterface $serializer
     * @param string $login
     * @param string $password
     */
    public function __construct(
        ClientInterface $httpClient,
        SerializerInterface $serializer,
        string $login,
        string $password
    ) {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    protected function sendRequest(Request $request): Response
    {
        $request
            ->setLogin($this->login)
            ->setPassword($this->password);

        try {
            $response = $this->httpClient->request(
                'POST',
                '',
                $this->extractOptions($request)
            );
        } catch (GuzzleException $e) {
            throw new RequestException($e, $request);
        }

        return $this->deserialize($request, $response);
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function extractOptions(Request $request): array
    {
        $data = $this->serializer->serialize($request, 'json');

        return [
            'headers' => ['Content-Type' => 'application/json'],
            'query' => ['data' => $data],
        ];
    }

    /**
     * @param Request $request
     * @param ResponseInterface $response
     * @return Response
     * @throws Exception
     */
    protected function deserialize(Request $request, ResponseInterface $response): Response
    {
        if (!$this->isJsonResponseBody($response)) {
            throw new ResponseException($response, $request);
        }

        $responseBody = (string)$response->getBody();

        if ($this->isErrorResponse($response)) {
            $errorResponse = $this->serializer->deserialize($responseBody, ErrorResponse::class, 'json');
            throw new ErrorResponseException($errorResponse, $request);
        }

        return $this->serializer->deserialize($responseBody, $request->responseClassName(), 'json');
    }

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    protected function isJsonResponseBody(ResponseInterface $response): bool
    {
        $str = (string)$response->getBody();
        $json = json_decode($str);
        return $json && $str !== $json;
    }

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    protected function isErrorResponse(ResponseInterface $response): bool
    {
        $responseBody = (string)$response->getBody();
        return mb_strpos($responseBody,'"status":false') !== false;
    }
}