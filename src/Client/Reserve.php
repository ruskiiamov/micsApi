<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Client;

use Exception;
use LaptopDev\MicsApi\Request\Reserve\AddGoodsReserveRequest;
use LaptopDev\MicsApi\Request\Reserve\EditGoodsReserveRequest;
use LaptopDev\MicsApi\Request\Reserve\GetContractsRequest;
use LaptopDev\MicsApi\Request\Reserve\GetReserveInfoRequest;
use LaptopDev\MicsApi\Request\Reserve\GetStocksInfoRequest;
use LaptopDev\MicsApi\Request\Reserve\RemoveGoodsReserveRequest;
use LaptopDev\MicsApi\Response\Reserve\AddGoodsReserveResponse;
use LaptopDev\MicsApi\Response\Reserve\EditGoodsReserveResponse;
use LaptopDev\MicsApi\Response\Reserve\GetContractsResponse;
use LaptopDev\MicsApi\Response\Reserve\GetReserveInfoResponse;
use LaptopDev\MicsApi\Response\Reserve\GetStocksInfoResponse;
use LaptopDev\MicsApi\Response\Reserve\RemoveGoodsReserveResponse;

class Reserve extends Client
{
    /**
     * @param GetStocksInfoRequest $request
     * @return GetStocksInfoResponse
     * @throws Exception
     */
    public function sendGetStocksInfoRequest(GetStocksInfoRequest $request): GetStocksInfoResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetContractsRequest $request
     * @return GetContractsResponse
     * @throws Exception
     */
    public function sendGetContractsRequest(GetContractsRequest $request): GetContractsResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetReserveInfoRequest $request
     * @return GetReserveInfoResponse
     * @throws Exception
     */
    public function sendGetReserveInfoRequest(GetReserveInfoRequest $request): GetReserveInfoResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param AddGoodsReserveRequest $request
     * @return AddGoodsReserveResponse
     * @throws Exception
     */
    public function sendAddGoodsReserveRequest(AddGoodsReserveRequest $request): AddGoodsReserveResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param EditGoodsReserveRequest $request
     * @return EditGoodsReserveResponse
     * @throws Exception
     */
    public function sendEditGoodsReserveRequest(EditGoodsReserveRequest $request): EditGoodsReserveResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param RemoveGoodsReserveRequest $request
     * @return RemoveGoodsReserveResponse
     * @throws Exception
     */
    public function sendRemoveGoodsReserveRequest(RemoveGoodsReserveRequest $request): RemoveGoodsReserveResponse
    {
        return $this->sendRequest($request);
    }
}