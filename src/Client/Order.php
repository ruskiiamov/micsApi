<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Client;

use Exception;
use LaptopDev\MicsApi\Request\Order\DeleteOrderRequest;
use LaptopDev\MicsApi\Request\Order\GetConsigneesRequest;
use LaptopDev\MicsApi\Request\Order\GetOrderPositionsRequest;
use LaptopDev\MicsApi\Request\Order\GetShippingAddressRequest;
use LaptopDev\MicsApi\Request\Order\MakeOrderRequest;
use LaptopDev\MicsApi\Request\Order\MakeQuickOrderRequest;
use LaptopDev\MicsApi\Response\Order\DeleteOrderResponse;
use LaptopDev\MicsApi\Response\Order\GetConsigneesResponse;
use LaptopDev\MicsApi\Response\Order\GetOrderPositionsResponse;
use LaptopDev\MicsApi\Response\Order\GetShippingAddressResponse;
use LaptopDev\MicsApi\Response\Order\MakeOrderResponse;
use LaptopDev\MicsApi\Response\Order\MakeQuickOrderResponse;

class Order extends Client
{
    /**
     * @param MakeOrderRequest $request
     * @return MakeOrderResponse
     * @throws Exception
     */
    public function sendMakeOrderRequest(MakeOrderRequest $request): MakeOrderResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param MakeQuickOrderRequest $request
     * @return MakeQuickOrderResponse
     * @throws Exception
     */
    public function sendMakeQuickOrderRequest(MakeQuickOrderRequest $request): MakeQuickOrderResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetConsigneesRequest $request
     * @return GetConsigneesResponse
     * @throws Exception
     */
    public function sendGetConsigneesRequest(GetConsigneesRequest $request): GetConsigneesResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetShippingAddressRequest $request
     * @return GetShippingAddressResponse
     * @throws Exception
     */
    public function sendGetShippingAddressRequest(GetShippingAddressRequest $request): GetShippingAddressResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param DeleteOrderRequest $request
     * @return DeleteOrderResponse
     * @throws Exception
     */
    public function sendDeleteOrderRequest(DeleteOrderRequest $request): DeleteOrderResponse
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetOrderPositionsRequest $request
     * @return GetOrderPositionsResponse
     * @throws Exception
     */
    public function sendGetOrderPositionsRequest(GetOrderPositionsRequest $request): GetOrderPositionsResponse
    {
        return $this->sendRequest($request);
    }
}