<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Order\GetOrderPositionsResponse;

class GetOrderPositionsRequest extends AbstractRequest
{
    const RESPONSE = GetOrderPositionsResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'GetOrderPositions';

    /**
     * @JMS\SerializedName("order")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $order;

    /**
     * @param string $order
     */
    public function __construct(string $order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function order(): string
    {
        return $this->order;
    }
}