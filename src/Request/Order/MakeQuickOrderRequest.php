<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Common\Good;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Order\MakeQuickOrderResponse;

class MakeQuickOrderRequest extends AbstractRequest
{
    const RESPONSE = MakeQuickOrderResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'MakeQuickOrder';

    /**
     * @JMS\SerializedName("goods")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\Good>")
     * @Required
     *
     * @var Good[]
     */
    private $goods;

    /**
     * @JMS\SerializedName("consignee")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $consignee;

    /**
     * @JMS\SerializedName("delivery")
     * @JMS\Type("int")
     *
     * @var int|null
     */
    private $delivery;

    /**
     * @JMS\SerializedName("shipping")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $shipping;

    /**
     * @JMS\SerializedName("contract")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $contract;

    /**
     * @JMS\SerializedName("paymentcondition")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $paymentCondition;

    /**
     * @JMS\SerializedName("mode")
     * @JMS\Type("int")
     *
     * @var int|null
     */
    private $mode;

    /**
     * @JMS\SerializedName("comments")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $comments;

    /**
     * @param string $contract
     * @param string $consignee
     * @param Good[] $goods
     */
    public function __construct(string $contract, string $consignee, array $goods)
    {
        $this->contract = $contract;
        $this->consignee = $consignee;
        $this->goods = $goods;
    }

    /**
     * @return Good[]
     */
    public function goods(): array
    {
        return $this->goods;
    }

    /**
     * @param Good $good
     * @return $this
     */
    public function addGood(Good $good): self
    {
        $this->goods[] = $good;
        return $this;
    }

    /**
     * @return string
     */
    public function consignee(): string
    {
        return $this->consignee;
    }

    /**
     * @return int|null
     */
    public function delivery(): ?int
    {
        return $this->delivery;
    }

    /**
     * @param int $delivery
     * @return $this
     */
    public function setDelivery(int $delivery): self
    {
        $this->delivery = $delivery;
        return $this;
    }

    /**
     * @return string|null
     */
    public function shipping(): ?string
    {
        return $this->shipping;
    }

    /**
     * @param string $shipping
     * @return $this
     */
    public function setShipping(string $shipping): self
    {
        $this->shipping = $shipping;
        return $this;
    }

    /**
     * @return string|null
     */
    public function contract(): ?string
    {
        return $this->contract;
    }

    /**
     * @return string|null
     */
    public function paymentCondition(): ?string
    {
        return $this->paymentCondition;
    }

    /**
     * @param string $paymentCondition
     * @return $this
     */
    public function setPaymentCondition(string $paymentCondition): self
    {
        $this->paymentCondition = $paymentCondition;
        return $this;
    }

    /**
     * @return int|null
     */
    public function mode(): ?int
    {
        return $this->mode;
    }

    /**
     * @param int $mode
     * @return $this
     */
    public function setMode(int $mode): self
    {
        $this->mode = $mode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function comments(): ?string
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     * @return $this
     */
    public function setComments(string $comments): self
    {
        $this->comments = $comments;
        return $this;
    }
}