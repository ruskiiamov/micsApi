<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Order\GetConsigneesResponse;

class GetConsigneesRequest extends AbstractRequest
{
    const RESPONSE = GetConsigneesResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'GetConsignees';

    /**
     * @JMS\SerializedName("contract")
     * @JMS\Type("string")
     * @Required
     *
     * @var string|null
     */
    private $contract;

    /**
     * @param string $contract
     */
    public function __construct(string $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @return string
     */
    public function contract(): string
    {
        return $this->contract;
    }
}