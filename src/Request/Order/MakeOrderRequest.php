<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Order\MakeOrderResponse;

class MakeOrderRequest extends AbstractRequest
{
    const RESPONSE = MakeOrderResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'MakeOrder';

    /**
     * @JMS\SerializedName("consignee")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $consignee;

    /**
     * @JMS\SerializedName("delivery")
     * @JMS\Type("int")
     * @Required
     *
     * @var int
     */
    private $delivery;

    /**
     * @JMS\SerializedName("contract")
     * @JMS\Type("string")
     * @Required
     *
     * @var string|null
     */
    private $contract;

    /**
     * @JMS\SerializedName("paymentcondition")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $paymentCondition;

    /**
     * @JMS\SerializedName("mode")
     * @JMS\Type("int")
     *
     * @var int|null
     */
    private $mode;

    /**
     * @JMS\SerializedName("comments")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $comments;

    /**
     * @param string $consignee
     */
    public function __construct(string $contract, string $consignee, int $delivery)
    {
        $this->contract = $contract;
        $this->consignee = $consignee;
        $this->delivery = $delivery;
    }

    /**
     * @return string
     */
    public function consignee(): string
    {
        return $this->consignee;
    }

    /**
     * @return int
     */
    public function delivery(): int
    {
        return $this->delivery;
    }

    /**
     * @return string|null
     */
    public function contract(): ?string
    {
        return $this->contract;
    }

    /**
     * @return string|null
     */
    public function paymentCondition(): ?string
    {
        return $this->paymentCondition;
    }

    /**
     * @param string $paymentCondition
     * @return $this
     */
    public function setPaymentCondition(string $paymentCondition): self
    {
        $this->paymentCondition = $paymentCondition;
        return $this;
    }

    /**
     * @return int|null
     */
    public function mode(): ?int
    {
        return $this->mode;
    }

    /**
     * @param int $mode
     * @return $this
     */
    public function setMode(int $mode): self
    {
        $this->mode = $mode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function comments(): ?string
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     * @return $this
     */
    public function setComments(string $comments): self
    {
        $this->comments = $comments;
        return $this;
    }
}