<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Order\GetShippingAddressResponse;

class GetShippingAddressRequest extends AbstractRequest
{
    const RESPONSE = GetShippingAddressResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'GetShippingAddress';

    /**
     * @JMS\SerializedName("consignee")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $consignee;

    /**
     * @param string $consignee
     */
    public function __construct(string $consignee)
    {
        $this->consignee = $consignee;
    }

    /**
     * @return string
     */
    public function consignee(): string
    {
        return $this->consignee;
    }
}