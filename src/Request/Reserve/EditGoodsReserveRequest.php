<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Reserve;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Common\Good;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Reserve\EditGoodsReserveResponse;

class EditGoodsReserveRequest extends AbstractRequest
{
    const RESPONSE = EditGoodsReserveResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'EditGoodsReserve';

    /**
     * @JMS\SerializedName("contract")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $contract;

    /**
     * @JMS\SerializedName("goods")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\Good>")
     * @Required
     *
     * @var Good[]
     */
    private $goods;

    /**
     * @param string $contract
     * @param Good[] $goods
     */
    public function __construct(string $contract, array $goods)
    {
        $this->contract = $contract;
        $this->goods = $goods;
    }

    /**
     * @return string
     */
    public function contract(): string
    {
        return $this->contract;
    }

    /**
     * @return Good[]
     */
    public function goods(): array
    {
        return $this->goods;
    }

    /**
     * @param Good $good
     * @return $this
     */
    public function addGood(Good $good): self
    {
        $this->goods[] = $good;
        return $this;
    }
}