<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Reserve;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Reserve\GetReserveInfoResponse;

class GetReserveInfoRequest extends AbstractRequest
{
    const RESPONSE = GetReserveInfoResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'GetReserveInfo';

    /**
     * @JMS\SerializedName("contract")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $contract;

    /**
     * @param string $contract
     */
    public function __construct(string $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @return string
     */
    public function contract(): string
    {
        return $this->contract;
    }
}