<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Reserve;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Reserve\GetContractsResponse;

class GetContractsRequest extends AbstractRequest
{
    const RESPONSE = GetContractsResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'getContracts';
}