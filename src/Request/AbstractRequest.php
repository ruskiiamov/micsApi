<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Contract\Request;
use LaptopDev\MicsApi\Contract\Response;

abstract class AbstractRequest implements Request
{
    const RESPONSE = Response::class;

    /**
     * @JMS\SerializedName("login")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $login;

    /**
     * @JMS\SerializedName("password")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $password;

    /** @var string */
    static protected $method;

    /**
     * @inheritDoc
     */
    public function responseClassName(): string
    {
        return static::RESPONSE;
    }

    /**
     * @inheritDoc
     */
    public function setLogin(string $login): Request
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setPassword(string $password): Request
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function login(): string
    {
        return $this->login;
    }

    /**
     * @inheritDoc
     */
    public function password(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function method(): string
    {
        return static::$method;
    }
}