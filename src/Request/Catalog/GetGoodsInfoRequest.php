<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Catalog\GetGoodsInfoResponse;

class GetGoodsInfoRequest extends AbstractRequest
{
    const RESPONSE = GetGoodsInfoResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'GetGoodsInfo';

    /**
     * @JMS\SerializedName("goods")
     * @JMS\Type("array<int>")
     * @Required
     *
     * @var int[]
     */
    private $goods;

    /**
     * @JMS\SerializedName("contract")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $contract;

    /**
     * @JMS\SerializedName("paymentcondition")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $paymentCondition;

    /**
     * @param string $contract
     * @param int[] $goods
     */
    public function __construct(string $contract, array $goods)
    {
        $this->contract = $contract;
        $this->goods = $goods;
    }

    /**
     * @return int[]
     */
    public function goods(): array
    {
        return $this->goods;
    }

    /**
     * @param int $good
     * @return $this
     */
    public function addGood(int $good): self
    {
        $this->goods[] = $good;
        return $this;
    }

    /**
     * @return string
     */
    public function contract(): string
    {
        return $this->contract;
    }

    /**
     * @return string|null
     */
    public function paymentCondition(): ?string
    {
        return $this->paymentCondition;
    }

    /**
     * @param string $paymentCondition
     * @return $this
     */
    public function setPaymentCondition(string $paymentCondition): self
    {
        $this->paymentCondition = $paymentCondition;
        return $this;
    }
}