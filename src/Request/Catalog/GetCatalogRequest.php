<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Catalog\GetCatalogResponse;

class GetCatalogRequest extends AbstractRequest
{
    const RESPONSE = GetCatalogResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'getCatalog';

    /**
     * @JMS\SerializedName("contract")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $contract;

    /**
     * @JMS\SerializedName("paymentcondition")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $paymentCondition;

    /**
     * @JMS\SerializedName("group")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $group;

    /**
     * @JMS\SerializedName("vendor")
     * @JMS\Type("string")
     *
     * @var string|null
     */
    private $vendor;

    /**
     * @JMS\SerializedName("page")
     * @JMS\Type("int")
     *
     * @var int|null
     */
    private $page;

    /**
     * @JMS\SerializedName("deferred_delivery")
     * @JMS\Type("bool")
     *
     * @var bool|null
     */
    private $deferredDelivery;

    /**
     * @JMS\SerializedName("conditions_only")
     * @JMS\Type("bool")
     *
     * @var bool|null
     */
    private $conditionsOnly;

    /**
     * @param string $contract
     */
    public function __construct(string $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @return string
     */
    public function contract(): string
    {
        return $this->contract;
    }

    /**
     * @return string|null
     */
    public function paymentCondition(): ?string
    {
        return $this->paymentCondition;
    }

    /**
     * @param string $paymentCondition
     * @return $this
     */
    public function setPaymentCondition(string $paymentCondition): self
    {
        $this->paymentCondition = $paymentCondition;
        return $this;
    }

    /**
     * @return string|null
     */
    public function group(): ?string
    {
        return $this->group;
    }

    /**
     * @param string $group
     * @return $this
     */
    public function setGroup(string $group): self
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return string|null
     */
    public function vendor(): ?string
    {
        return $this->vendor;
    }

    /**
     * @param string $vendor
     * @return $this
     */
    public function setVendor(string $vendor): self
    {
        $this->vendor = $vendor;
        return $this;
    }

    /**
     * @return int|null
     */
    public function page(): ?int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return $this
     */
    public function setPage(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function deferredDelivery(): ?bool
    {
        return $this->deferredDelivery;
    }

    /**
     * @param bool $deferredDelivery
     * @return $this
     */
    public function setDeferredDelivery(bool $deferredDelivery): self
    {
        $this->deferredDelivery = $deferredDelivery;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function conditionsOnly(): ?bool
    {
        return $this->conditionsOnly;
    }

    /**
     * @param bool $conditionsOnly
     * @return $this
     */
    public function setConditionsOnly(bool $conditionsOnly): self
    {
        $this->conditionsOnly = $conditionsOnly;
        return $this;
    }
}