<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Request\Catalog;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\MicsApi\Request\AbstractRequest;
use LaptopDev\MicsApi\Response\Catalog\GetHierarchyResponse;

class GetHierarchyRequest extends AbstractRequest
{
    const RESPONSE = GetHierarchyResponse::class;

    /**
     * @JMS\SerializedName("method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    protected static $method = 'GetHierarchy';
}