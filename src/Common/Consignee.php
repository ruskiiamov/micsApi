<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class Consignee
{
    /**
     * @JMS\SerializedName("text")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $text;

    /**
     * @JMS\SerializedName("value")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $value;

    /**
     * @return string
     */
    public function text(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }
}