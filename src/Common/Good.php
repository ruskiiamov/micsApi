<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class Good
{
    /**
     * @JMS\SerializedName("id")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $id;

    /**
     * @JMS\SerializedName("quantity")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $quantity;

    /**
     * @JMS\SerializedName("measure")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $measure;

    /**
     * @JMS\SerializedName("stock")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $stock;

    /**
     * @JMS\SerializedName("name")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $name;

    /**
     * @JMS\SerializedName("price")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $price;

    /**
     * @JMS\SerializedName("currency")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $currency;

    /**
     * @JMS\SerializedName("quality")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $quality;

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function quantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return string
     */
    public function measure(): string
    {
        return $this->measure;
    }

    /**
     * @return string
     */
    public function stock(): string
    {
        return $this->stock;
    }

    /**
     * @param string $stock
     * @return $this
     */
    public function setStock(string $stock): self
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function currency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function quality(): string
    {
        return $this->quality;
    }

    /**
     * @param string $quality
     * @return $this
     */
    public function setQuality(string $quality): self
    {
        $this->quality = $quality;
        return $this;
    }
}