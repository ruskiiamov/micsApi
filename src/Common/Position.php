<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class Position
{
    /**
     * @JMS\SerializedName("id")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $id;

    /**
     * @JMS\SerializedName("PartNumber")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $partNumber;

    /**
     * @JMS\SerializedName("Name")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $name;

    /**
     * @JMS\SerializedName("TerminalId")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $terminalId;

    /**
     * @JMS\SerializedName("Quality")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $quality;

    /**
     * @JMS\SerializedName("QualityName")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $qualityName;

    /**
     * @JMS\SerializedName("Price")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $price;

    /**
     * @JMS\SerializedName("Cost")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $cost;

    /**
     * @JMS\SerializedName("Currency")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $currency;

    /**
     * @JMS\SerializedName("Quantity")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $quantity;

    /**
     * @JMS\SerializedName("Reserved")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $reserved;

    /**
     * @JMS\SerializedName("Measure")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $measure;

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function partNumber(): string
    {
        return $this->partNumber;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function terminalId(): string
    {
        return $this->terminalId;
    }

    /**
     * @return string
     */
    public function quality(): string
    {
        return $this->quality;
    }

    /**
     * @return string
     */
    public function qualityName(): string
    {
        return $this->qualityName;
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function cost(): float
    {
        return $this->cost;
    }

    /**
     * @return string
     */
    public function currency(): string
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function quantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function reserved(): int
    {
        return $this->reserved;
    }

    /**
     * @return string
     */
    public function measure(): string
    {
        return $this->measure;
    }
}