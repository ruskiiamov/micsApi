<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class ShippingAddress
{
    /**
     * @JMS\SerializedName("code")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $code;

    /**
     * @JMS\SerializedName("country")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $country;

    /**
     * @JMS\SerializedName("region_name")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $regionName;

    /**
     * @JMS\SerializedName("city_name")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $cityName;

    /**
     * @JMS\SerializedName("street")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $street;

    /**
     * @JMS\SerializedName("house_no")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $houseNo;

    /**
     * @JMS\SerializedName("deliv_addr_comment")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $delivAddrComment;

    /**
     * @JMS\SerializedName("ship_agent")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $shipAgent;

    /**
     * @JMS\SerializedName("ship_agent_term")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $shipAgentTerm;

    /**
     * @JMS\SerializedName("contacts")
     * @JMS\Type("array<LaptopDev\MicsApi\Common\Contact>")
     *
     * @var Contact[]
     */
    private $contacts;

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function country(): string
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function regionName(): string
    {
        return $this->regionName;
    }

    /**
     * @return string
     */
    public function cityName(): string
    {
        return $this->cityName;
    }

    /**
     * @return string
     */
    public function street(): string
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function houseNo(): string
    {
        return $this->houseNo;
    }

    /**
     * @return string
     */
    public function delivAddrComment(): string
    {
        return $this->delivAddrComment;
    }

    /**
     * @return string
     */
    public function shipAgent(): string
    {
        return $this->shipAgent;
    }

    /**
     * @return string
     */
    public function shipAgentTerm(): string
    {
        return $this->shipAgentTerm;
    }

    /**
     * @return Contact[]
     */
    public function contacts(): array
    {
        return $this->contacts;
    }
}