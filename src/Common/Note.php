<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class Note
{
    /**
     * @JMS\SerializedName("PrepaymentOrderCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $prepaymentOrderCode;

    /**
     * @JMS\SerializedName("CreditOrderCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $creditOrderCode;

    /**
     * @return string
     */
    public function prepaymentOrderCode(): string
    {
        return $this->prepaymentOrderCode;
    }

    /**
     * @return string
     */
    public function creditOrderCode(): string
    {
        return $this->creditOrderCode;
    }
}