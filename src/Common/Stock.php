<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class Stock
{
    /**
     * @JMS\SerializedName("num")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $num;

    /**
     * @JMS\SerializedName("price")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $price;

    /**
     * @return string
     */
    public function num(): string
    {
        return $this->num;
    }

    /**
     * @return string
     */
    public function price(): string
    {
        return $this->price;
    }
}