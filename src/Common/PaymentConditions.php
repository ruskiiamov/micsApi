<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class PaymentConditions
{
    /**
     * @JMS\SerializedName("Prepayment")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $prepayment;

    /**
     * @JMS\SerializedName("CreditDays")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $creditDays;

    /**
     * @return int
     */
    public function prepayment(): int
    {
        return $this->prepayment;
    }

    /**
     * @return int
     */
    public function creditDays(): int
    {
        return $this->creditDays;
    }
}