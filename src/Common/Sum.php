<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class Sum
{
    /**
     * @JMS\SerializedName("currency")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $currency;

    /**
     * @JMS\SerializedName("sum")
     * @JMS\Type("float")
     *
     * @var float
     */
    private $sum;

    /**
     * @return string
     */
    public function currency(): string
    {
        return $this->currency;
    }

    /**
     * @return float
     */
    public function sum(): float
    {
        return $this->sum;
    }
}