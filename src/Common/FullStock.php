<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class FullStock
{
    /**
     * @JMS\SerializedName("НазваниеСклада")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $stockName;

    /**
     * @JMS\SerializedName("Остаток")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $rest;

    /**
     * @JMS\SerializedName("ДоступноДляЗаказа")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $available;

    /**
     * @JMS\SerializedName("Цена")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $price;

    /**
     * @JMS\SerializedName("Валюта")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $currency;

    /**
     * @JMS\SerializedName("ОжидаемыйПриход")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $expectedIncome;

    /**
     * @JMS\SerializedName("ВидОценки")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $estimationType;

    /**
     * @JMS\SerializedName("КодВидаОценки")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $estimationTypeCode;

    /**
     * @JMS\SerializedName("ИмяВидаОценки")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $estimationTypeName;

    /**
     * @return string
     */
    public function stockName(): string
    {
        return $this->stockName;
    }

    /**
     * @return string
     */
    public function rest(): string
    {
        return $this->rest;
    }

    /**
     * @return string
     */
    public function available(): string
    {
        return $this->available;
    }

    /**
     * @return string
     */
    public function price(): string
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function currency(): string
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function expectedIncome(): int
    {
        return $this->expectedIncome;
    }

    /**
     * @return string
     */
    public function estimationType(): string
    {
        return $this->estimationType;
    }

    /**
     * @return string
     */
    public function estimationTypeCode(): string
    {
        return $this->estimationTypeCode;
    }

    /**
     * @return string
     */
    public function estimationTypeName(): string
    {
        return $this->estimationTypeName;
    }
}