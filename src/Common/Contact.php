<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class Contact
{
    /**
     * @JMS\SerializedName("NAME")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $name;

    /**
     * @JMS\SerializedName("PHONE")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $phone;

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function phone(): string
    {
        return $this->phone;
    }
}