<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class Contract
{
    /**
     * @JMS\SerializedName("ContractSapCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $contractSapCode;

    /**
     * @JMS\SerializedName("PayerSapCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $payerSapCode;

    /**
     * @JMS\SerializedName("ContractName")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $contractName;

    /**
     * @JMS\SerializedName("SalesOrg")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $salesOrg;

    /**
     * @JMS\SerializedName("ContractCurrency")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $contractCurrency;

    /**
     * @JMS\SerializedName("PaymentConditions")
     * @JMS\Type("LaptopDev\MicsApi\Common\PaymentConditions")
     *
     * @var PaymentConditions
     */
    private $paymentConditions;

    /**
     * @return string
     */
    public function contractSapCode(): string
    {
        return $this->contractSapCode;
    }

    /**
     * @return string
     */
    public function payerSapCode(): string
    {
        return $this->payerSapCode;
    }

    /**
     * @return string
     */
    public function contractName(): string
    {
        return $this->contractName;
    }

    /**
     * @return string
     */
    public function salesOrg(): string
    {
        return $this->salesOrg;
    }

    /**
     * @return string
     */
    public function contractCurrency(): string
    {
        return $this->contractCurrency;
    }

    /**
     * @return PaymentConditions
     */
    public function paymentConditions(): PaymentConditions
    {
        return $this->paymentConditions;
    }
}