<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class GoodInfo
{
    /**
     * @JMS\SerializedName("id")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $id;

    /**
     * @JMS\SerializedName("stock")
     * @JMS\Type("array<string, LaptopDev\MicsApi\Common\Stock>")
     *
     * @var Stock[]
     */
    private $stock;

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return Stock[]
     */
    public function stock(): array
    {
        return $this->stock;
    }
}