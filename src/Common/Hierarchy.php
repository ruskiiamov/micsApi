<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class Hierarchy
{
    /**
     * @JMS\SerializedName("КодГруппы")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $groupCode;

    /**
     * @JMS\SerializedName("Уровень")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $level;

    /**
     * @JMS\SerializedName("Название")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function groupCode(): string
    {
        return $this->groupCode;
    }

    /**
     * @return string
     */
    public function level(): string
    {
        return $this->level;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}