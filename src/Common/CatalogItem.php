<?php

declare(strict_types=1);

namespace LaptopDev\MicsApi\Common;

use JMS\Serializer\Annotation as JMS;

class CatalogItem
{
    /**
     * @JMS\SerializedName("КодТовараSAP")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $goodCodeSap;

    /**
     * @JMS\SerializedName("КодГруппы")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $groupCode;

    /**
     * @JMS\SerializedName("Категория")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $category;

    /**
     * @JMS\SerializedName("Подкатегория")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $subCategory;

    /**
     * @JMS\SerializedName("Партномер")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $partNumber;

    /**
     * @JMS\SerializedName("Ean")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $ean;

    /**
     * @JMS\SerializedName("КраткоеНаименование")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $shortName;

    /**
     * @JMS\SerializedName("ПолноеНаименование")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $fullName;

    /**
     * @JMS\SerializedName("КраткоеОписание")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $shortDescription;

    /**
     * @JMS\SerializedName("КодБренда")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $brandCode;

    /**
     * @JMS\SerializedName("Бренд")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $brand;

    /**
     * @JMS\SerializedName("КодТипаТовара")
     * @JMS\Type("int")
     *
     * @var int
     */
    private $goodTypeCode;

    /**
     * @JMS\SerializedName("ТипТовара")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $goodType;

    /**
     * @JMS\SerializedName("Распродажа")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $discount;

    /**
     * @JMS\SerializedName("Новинка")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $new;

    /**
     * @JMS\SerializedName("Эксклюзив")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $exclusive;

    /**
     * @JMS\SerializedName("Описание")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $description;

    /**
     * @JMS\SerializedName("КодМодельногоРяда")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $modelLineCode;

    /**
     * @JMS\SerializedName("МодельныйРяд")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $modelLine;

    /**
     * @JMS\SerializedName("Комплект")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $kit;

    /**
     * @JMS\SerializedName("СкладыНЕЗ_ПОВ_УП")
     * @JMS\Type("array<string, LaptopDev\MicsApi\Common\FullStock>")
     *
     * @var FullStock[]
     */
    private $stocksDamagedBox;

    /**
     * @JMS\SerializedName("СкладыНЕЗ_ПОВ_УП_ИмяВидаОценки")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $stocksDamagedBoxEstimationTypeName;

    /**
     * @JMS\SerializedName("СкладыБРАК")
     * @JMS\Type("array<string, LaptopDev\MicsApi\Common\FullStock>")
     *
     * @var FullStock[]
     */
    private $stocksRejected;

    /**
     * @JMS\SerializedName("СкладыБРАК_ИмяВидаОценки")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $stocksRejectedEstimationTypeName;

    /**
     * @JMS\SerializedName("Склады")
     * @JMS\Type("array<string, LaptopDev\MicsApi\Common\FullStock>")
     *
     * @var FullStock[]
     */
    private $stocks;

    /**
     * @return int
     */
    public function goodCodeSap(): int
    {
        return $this->goodCodeSap;
    }

    /**
     * @return string
     */
    public function groupCode(): string
    {
        return $this->groupCode;
    }

    /**
     * @return string
     */
    public function category(): string
    {
        return $this->category;
    }

    /**
     * @return string
     */
    public function subCategory(): string
    {
        return $this->subCategory;
    }

    /**
     * @return string
     */
    public function partNumber(): string
    {
        return $this->partNumber;
    }

    /**
     * @return string
     */
    public function ean(): string
    {
        return $this->ean;
    }

    /**
     * @return string
     */
    public function shortName(): string
    {
        return $this->shortName;
    }

    /**
     * @return string
     */
    public function fullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function shortDescription(): string
    {
        return $this->shortDescription;
    }

    /**
     * @return string
     */
    public function brandCode(): string
    {
        return $this->brandCode;
    }

    /**
     * @return string
     */
    public function brand(): string
    {
        return $this->brand;
    }

    /**
     * @return int
     */
    public function goodTypeCode(): int
    {
        return $this->goodTypeCode;
    }

    /**
     * @return string
     */
    public function goodType(): string
    {
        return $this->goodType;
    }

    /**
     * @return string
     */
    public function discount(): string
    {
        return $this->discount;
    }

    /**
     * @return string
     */
    public function new(): string
    {
        return $this->new;
    }

    /**
     * @return string
     */
    public function exclusive(): string
    {
        return $this->exclusive;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function modelLineCode(): string
    {
        return $this->modelLineCode;
    }

    /**
     * @return string
     */
    public function modelLine(): string
    {
        return $this->modelLine;
    }

    /**
     * @return string
     */
    public function kit(): string
    {
        return $this->kit;
    }

    /**
     * @return FullStock[]
     */
    public function stocksDamagedBox(): array
    {
        return $this->stocksDamagedBox;
    }

    /**
     * @return string
     */
    public function stocksDamagedBoxEstimationTypeName(): string
    {
        return $this->stocksDamagedBoxEstimationTypeName;
    }

    /**
     * @return FullStock[]
     */
    public function stocksRejected(): array
    {
        return $this->stocksRejected;
    }

    /**
     * @return string
     */
    public function stocksRejectedEstimationTypeName(): string
    {
        return $this->stocksRejectedEstimationTypeName;
    }

    /**
     * @return FullStock[]
     */
    public function stocks(): array
    {
        return $this->stocks;
    }
}