# MicsApi
## КЛИЕНТ
Необходимо указать адрес сервиса, логин и пароль
#### Пример:
```php
use LaptopDev\MicsApi\MicsApiClient;

$client = new MicsApiClient(
    'https://www.mics.ru/api/',
    'your_login',
    'your_password'
);
```
## API КАТАЛОГА
### Получение иерархии каталога
- метод API: GetHierarchy
- входных параметров нет
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Catalog\GetHierarchyRequest;

$request = new GetHierarchyRequest();

$response = $client->catalog->sendGetHierarchyRequest($request);
```
### Получение каталога
- метод API: GetCatalog
- обязательный входной параметр: внутренний номер договора
- необязательные входные параметры: условие платежа по договору, код группы,
  код вендора, номер страницы, флаг отсроченной поставки, флаг некондиции
- примечание: время ожидания ответа примерно 2 минуты
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Catalog\GetCatalogRequest;

$request = new GetCatalogRequest('your_contract');
$request
    ->setPaymentCondition('your_payment_condition')
    ->setGroup('your_group_code')
    ->setVendor('your_vendor')
    ->setPage(1)
    ->setDeferredDelivery(false)
    ->setConditionsOnly(true);

$response = $client->catalog->sendGetCatalogRequest($request);
```
### Получение цены и сведений о наличии товара на складах
- метод API: GetGoodsInfo
- обязательные входные параметры: внутренний номер договора, список идентификаторов товаров (не более 100 товаров)
- необязательные входные параметры: условие платежа по договору
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Catalog\GetGoodsInfoRequest;

$request = new GetGoodsInfoRequest('your_contract', [12345, 54321]);
$request
    ->setPaymentCondition('your_payment_condition');
    
$response = $client->catalog->sendGetGoodsInfoRequest($request);
```
## API РЕЗЕРВА
### Получение складов, доступных для резерва 
- метод API: GetStocksInfo
- входных параметров нет
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Reserve\GetStocksInfoRequest;

$request = new GetStocksInfoRequest();

$response = $client->reserve->sendGetStocksInfoRequest($request);
```
### Получение доступных договоров
- метод API: GetContracts
- входных параметров нет
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Reserve\GetContractsRequest;

$request = new GetContractsRequest();

$response = $client->reserve->sendGetContractsRequest($request);
```
### Получение списка товаров, находящихся в резерве (корзина)
- метод API: GetReserveInfo
- обязательный входной параметр: внутренний номер договора
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Reserve\GetReserveInfoRequest;

$request = new GetReserveInfoRequest('your_contract');

$response = $client->reserve->sendGetReserveInfoRequest($request);
```
### Добавление товаров в резерв (корзину)
- метод API: AddGoodsReserve
- обязательные входные параметры: внутренний номер договора, массив объектов Good (должны содержать код товара, 
количество и код склада)
- примечание: за 1 запрос можно передать не более 100 позиций товаров, каждая позиция может содержать не более 
5 единиц товара
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Common\Good;
use LaptopDev\MicsApi\Request\Reserve\AddGoodsReserveRequest;

$good = new Good();
$good
    ->setId('good_id')
    ->setQuantity(5)
    ->setStock('your_stock');

$request = new AddGoodsReserveResponse('your_contract', [$good]);

$response = $client->reserve->sendAddGoodsReserveResponse($request);
```
### Изменение товаров в резерве (корзине)
- метод API: EditGoodsReserve
- обязательные входные параметры: внутренний номер договора, массив объектов Good (должны содержать код товара, 
количество и код склада)
- примечание: за 1 запрос можно передать не более 100 позиций товаров, каждая позиция может содержать не более
5 единиц товара
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Common\Good;
use LaptopDev\MicsApi\Request\Reserve\EditGoodsReserveRequest;

$good = new Good();
$good
    ->setId('good_id')
    ->setQuantity(5)
    ->setStock('your_stock');

$request = new EditGoodsReserveRequest('your_contract', [$good]);

$response = $client->reserve->sendEditGoodsReserveRequest($request);
```
### Удаление товаров из резерае (корзины)
- метод API: RemoveGoodsReserve
- обязательные входные параметры: внутренний номер договора, массив объектов Good (должны содержать код товара)
- примечание: за 1 запрос можно передать не более 100 позиций товаров
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Common\Good;
use LaptopDev\MicsApi\Request\Reserve\RemoveGoodsReserveRequest;

$good = new Good();
$good
    ->setId('good_id');

$request = new RemoveGoodsReserveRequest('your_contract', [$good]);

$response = $client->reserve->sendRemoveGoodsReserveRequest($request);
```
## API ЗАКАЗА
### Создание заказа из зарезервированных товаров (из корзины)
- метод API: MakeOrder
- обязательные входные параметры: внутренний номер договора, номер грузополучателя, доставка 
(1 - самовывоз, 2 - доставка)
- необязательные входные параметры: условие платежа по договору, способ оформления заказа, комментарий к заказу
- примечание: способ оформления заказа - значение 1 - передать заказ менеджеру для дальнейшей обработки, 2 - 
окончательно сформировать заказ (пока доступен только 1)
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Order\MakeOrderRequest;

$request = new MakeOrderRequest('your_contract', 'your_consignee', 1);
$request
    ->setPaymentCondition('your_paymentCondition')
    ->setMode(1)
    ->setComments('your_comments');

$response = $client->order->sendMakeOrderRequest($request);
```
### Создание заказа типа "Предложение", минуя корзину
- метод API: MakeQuickOrder
- обязательные входные параметры: внутренний номер договора, номер грузополучателя, массив объектов Good (должны 
содержать код товара, количество и код склада)
- необязательные входные параметры: доставка (1 - самовывоз, 2 - доставка), номер карточки адреса доставки, условие
платежа по договору, способ оформления заказа, комментарий к заказу
- примечание: способ оформления заказа - значение 1 - передать заказ менеджеру для дальнейшей обработки, 2 - 
окончательно сформировать заказ (пока доступен только 1)
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Common\Good;
use LaptopDev\MicsApi\Request\Order\MakeQuickOrderRequest;

$good = new Good();
$good
    ->setId('good_id')
    ->setQuantity(5)
    ->setStock('your_stock');

$request = new MakeQuickOrderRequest('your_contract', 'your_consignee', [$good]);
$request
    ->setDelivery(1)
    ->setShipping('your_shipping')
    ->setPaymentCondition('your_paymentCondition')
    ->setMode(1)
    ->setComments('your_comments');

$response = $client->order->sendMakeQuickOrderRequest($request);
```
### Получение грузополучателей и условий платежа
- метод API: GetConsignees
- обязательный входной параметр: внутренний номер договора
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Order\GetConsigneesRequest;

$request = new GetConsigneesRequest('your_contract');

$response = $client->order->sendGetConsigneesRequest($request);
```
### Получение карточки  доставки грузополучателя (для способа отгрузки "Доставка")
- метод API: GetShippingAddress
- обязательный входной параметр: номер грузополучателя
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Order\GetShippingAddressRequest;

$request = new GetShippingAddressRequest('your_consignee');

$response = $client->order->sendGetShippingAddressRequest($request);
```
### Удаление (аннулирование) заказа
- метод API: DeleteOrder
- обязательный входной параметр: номер заказа
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Order\DeleteOrderRequest;

$request = new DeleteOrderRequest('your_order');

$response = $client->order->sendDeleteOrderRequest($request);
```
### Получение содержимого заказа
- метод API: GetOrderPositions
- обязательный входной параметр: номер заказа
##### Пример выполнения запроса:
```php
use LaptopDev\MicsApi\Request\Order\GetOrderPositionsRequest;

$request = new GetOrderPositionsRequest('your_order');

$response = $client->order->sendGetOrderPositionsRequest($request);
```